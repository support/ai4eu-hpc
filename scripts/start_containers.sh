#!/bin/bash

NODE=""

if [ -n $1 ]
then
	mapfile -t NODES < $1
else 
	NODE="0.0.0.0"
fi

APPS=$(head  deployments/*deployment*| grep app: | cut -d " " -f 6)
APPS=$(echo $APPS | sed "s/ /,/g")

IFS=","

IMAGES=(tmp/*.sif)
HOST=$(hostname)
HOSTIP=$(nslookup $HOST | grep "Address: " | awk '{print $2}')

for A in $APPS
do
	echo "Launching $A"
	if [ $PREDEFINED_PORT != "False" ]
	then
		PORT=$PREDEFINED_PORT
	else
		PORT=$(shuf -i 2000-65000 -n 1)
	fi

	#image iterator i
	i=0
        for image in $APPS
	do
		image=${IMAGES[$i]}
                echo $image | grep -i ${A}
                if [ $? == 0 ]
                then

			if [ -n $NODES ]
			then
				NODE=${NODES[$i]}
				NODE=$(nslookup $NODE | grep "Address: " | awk '{print $2}')
				echo $NODE
			fi


               	        sed -i "s/\"container_name\":\"$A\",\"ip_address\":\"$A\",\"port\":\"[0-9]*/\"container_name\":\"$A\",\"ip_address\":\"$NODE\",\"port\":\"$PORT/" dockerinfo.json

		        #CREATE CONFIG FILE
                        echo "{
                               \"grpcport\": $PORT
                        }" > tmp/work_dir/$A.conf
			
			#ENABLING INTERNET ACCESS
			EXPORT="export {http,HTTP,https,HTTPS,ftp,FTP,rsync,RSYNC,ntp,NTP}_proxy=http://localhost:8500"

	       	        if [ $A == "orchestrator" ]
                        then
                        	echo "Launching container $image on port $PORT"
				ORCHNODE=$NODE

				if [ $HOSTIP != $NODE ]
				then
					ssh -R 8500:localhost:8500 $ORCHNODE "sleep 50  && ulimit -Ss unlimited && cd $PWD && /apps/SINGULARITY/3.7.3/bin/singularity run -B tmp/work_dir:/orchestartor_container/work_dir $image &>orchestrator.log &" &
				else
					/apps/SINGULARITY/3.7.3/bin/singularity run -B tmp/work_dir:/orchestartor_container/work_dir $image &>orchestrator.log &
				fi
			else
				export CONFIG=/work_dir/$A.conf
                                echo "Launching container $image on port $PORT"

				if [ $HOSTIP != $NODE ]
                                then
					
					ssh -R 8500:localhost:8500 $NODE "$EXPORT && cd $PWD && export CONFIG=/work_dir/$A.conf && /apps/SINGULARITY/3.7.3/bin/singularity run -B tmp/work_dir:/work_dir $image &>$A.log &" &
				else
					/apps/SINGULARITY/3.7.3/bin/singularity run -B tmp/work_dir:/work_dir $image &>$A.log &
				fi
                        fi

                fi
                i=$(($i+1))
        done
        sleep 10
done


while sleep 50
do
	echo "runnning orchestrator"
	module load python/3.8.2
	python3 orchestrator_client/orchestrator_client.py -e $ORCHNODE:8061 --basepath=./
done

