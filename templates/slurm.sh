#!/bin/bash

#SBATCH --workdir=WORKDIR
#SBATCH --job-name=NAME
#SBATCH --output=%j.out
#SBATCH --error=%j.err
#SBATCH --ntasks-per-node=1
#SBATCH -n TASKS
##SBATCH --constraint=CONSTRAINT
#SBATCH --time=TIME


export PREDEFINED_PORT=PPORT

ulimit -Ss unlimited

scontrol show hostnames $SLURM_JOB_NODELIST > hostnames.txt

#ONLY if FORWARDING is set (add to config?)
ssh -fNL8500:localhost:8500 login2

./start_containers_test.sh hostnames.txt

