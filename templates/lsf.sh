#!/bin/bash

#BSUB -cwd WORKDIR
#BSUB -J NAME
#BSUB -o %J.out
#BSUB -e %J.err
#BSUB -R "span[ptile=1]"
#BSUB -n TASKS
#BSUB -W TIME


cp $LSB_DJOB_HOSTFILE hostnames.txt

./start_containers.sh hostnames.txt

