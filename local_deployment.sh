#!/bin/bash

IMAGES=$(cat deployments/*deployment*.yaml | grep image: | cut -d ' ' -f 10)
IMAGES=$(echo $IMAGES | sed "s/ /,/g")

APPS=$(head  deployments/*deployment*| grep app: | cut -d " " -f 6)
APPS=$(echo $APPS | sed "s/ /,/g")

IFS=","


echo Do docker images need to be downloaded? [y/n]
read Download

	if [ "$Download" == y ]

	then
		mkdir tmp
                mkdir tmp/work_dir

		echo "Downloading and converting images to singularity..."
	for I in $IMAGES
	do
		echo "Processing $I"
		docker run -v /var/run/docker.sock:/var/run/docker.sock \
		-v $PWD/tmp:/output \
		--privileged -t --rm \
		quay.io/singularity/docker2singularity \
		$I
	done
        elif [ "$Download" == n ]
	then
		echo "Using images under tmp/ ..."
	fi

i=0

IMAGES=(tmp/*.sif)

for A in $APPS
do

	PORT=$(shuf -i 2000-65000 -n 1)
	echo "Launching $A"
	
	echo "Launching container ${IMAGES[$i]} on port $PORT"
		sed -i "s/\"container_name\":\"$A\",\"ip_address\":\"$A\",\"port\":\"[0-9]*/\"container_name\":\"$A\",\"ip_address\":\"0.0.0.0\",\"port\":\"$PORT/" dockerinfo.json
	echo $A | grep -i orchestrator
        if [ $? == 0 ]
        then
                PORTORCH=$PORT
                echo "Orchestrator port is..$PORTORCH"
		ORCHIM=${IMAGES[$i]}
	else
		sed -i "s/\"container_name\":\"$A\",\"ip_address\":\"$A\",\"port\":\"[0-9]*/\"container_name\":\"$A\",\"ip_address\":\"0.0.0.0\",\"port\":\"$PORT/" dockerinfo.json
        	#CREATE CONFIG FILE
        	echo "{
    			\"grpcport\": $PORT
		}" > tmp/work_dir/$A.conf
        	export CONFIG=/work_dir/$A.conf
		singularity run -B tmp/work_dir:/work_dir ${IMAGES[$i]} &
        fi


	sleep 1
	i=$(($i+1))

	
done

singularity run -B tmp/work_dir:/orchestartor_container/work_dir $ORCHIM &
#We must change folder if we want to pick the correct dockerinfo file
cd orchestrator_client

while sleep 1
do
	python3 orchestrator_client.py 0.0.0.0:8061
done
