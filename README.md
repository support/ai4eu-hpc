Collection of scripts to deploy AI4EU acumos pipelines into BSC production clusters.

Testing based on the Sudoku-tutorial example.

# local_deplyment.sh

- Downloads the necessary docker images
- Converts them to singularity images
- Edits configuration files for a local/custom run
- Launches all components of the pipeline 

