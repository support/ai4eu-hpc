#!/bin/bash

#CODE INITIALIZATION

if [ -f "etc/remote_cluster.conf" ]
then
	echo "Configuration file exists." 
	echo "Using configuration file" $( realpath etc/remote_cluster.conf )
else
	echo "NO configuration file found. Running in interactive manner..."
fi

source etc/remote_cluster.conf


#SOLUTION EXTRACTION

if [ -z ${SOLUTION_FILE+x} ];
then
	echo "Please, enter solution package to load (solution.zip):"
	read SOLUTION_FILE
fi

if [ -f "$SOLUTION_FILE" ]
then
        echo "Loading solution file: $SOLUTION_FILE" 
else
        echo "ERROR: Solution file does not exist!!"
	echo "Exiting..."
	exit
fi


echo "Uncompressing solution..."

unzip $SOLUTION_FILE

#DOCKER IMAGE PREPARATION

IMAGES=$(cat deployments/*deployment*.yaml | grep image: | cut -d ' ' -f 10)
IMAGES=$(echo $IMAGES | sed "s/ /,/g")

APPS=$(head  deployments/*deployment*| grep app: | cut -d " " -f 6)
APPS=($APPS)

IFS=","


echo Do docker images need to be downloaded? [y/n]
read Download

if [ "$Download" == y ]
then

	mkdir tmp
	mkdir tmp/work_dir

	echo "Downloading and converting images to singularity..."
	
	i=0
	for I in $IMAGES
	do
		echo "Processing $I"
		echo $i
		echo ${APPS[$i]}
		docker run -v /var/run/docker.sock:/var/run/docker.sock \
		-v $PWD/tmp:/output \
		--privileged -t --rm \
		quay.io/singularity/docker2singularity \
		--name ${APPS[$i]} $I
		i=$(($i+1))
	done
elif [ "$Download" == n ]
then
	echo "Using images under tmp/ ..."
fi


#CLUSTER CONNECTION

CONNECTION=$( ssh $USERNAME@$CLUSTER hostname )

if [ "$?" -eq "0" ]
then
	echo "Successfully connected to cluster!"
else
	echo "Cluster connection error..."
	echo "Exiting"
	exit
fi

#WORKING DIR PREPARATION

CONNECTION=$( ssh $USERNAME@$CLUSTER cd $WORKDIR )

if [ "$?" -eq "0" ]
then
        echo "Uploading data to workdir..."

	scp $SOLUTION_FILE $USERNAME@$CLUSTER:$WORKDIR
	scp -r tmp $USERNAME@$CLUSTER:$WORKDIR
	ssh $USERNAME@$CLUSTER "cd $WORKDIR; unzip $SOLUTION_FILE"

	if [ "$?" -eq "0" ]
	then
		echo "Solution correctly uploaded"
	else
		echo "Error uploading solution..."
		echo "Exiting"
		exit
	fi
else
        echo "Directory: $WORKDIR does not exist. Create it?"
        read create
	if [ "$create" == "y" ]
	then
		$( ssh $USERNAME@$CLUSTER mkdir -p $WORKDIR )
		if [ "$?" -eq "0" ]
		then
			echo "Successfully created workdir $WORKDIR"
			echo "Uploading data to workdir..."

        		scp $SOLUTION_FILE $USERNAME@$CLUSTER:$WORKDIR
        		scp -r tmp $USERNAME@$CLUSTER:$WORKDIR
        		ssh $USERNAME@$CLUSTER "cd $WORKDIR; unzip $SOLUTION_FILE"

        		if [ "$?" -eq "0" ]
        		then
                		echo "Solution correctly uploaded"
        		else
                		echo "Error uploading solution..."
                		echo "Exiting"
                		exit
			fi
		else
			echo "Error creating directory $WORKDIR"
			echo "Exiting"
        	        exit
		fi
	else
		echo "Refusing to create $WORKDIR"
                echo "Exiting"
                exit
	fi
fi

#JOB PREPARATION

echo " Creating job script from template ($SCHEDULER)"

cp templates/$SCHEDULER.sh $SCHEDULER.sh

if [ "$SCHEDULER" == "slurm" ]
then

sed -i "s/WORKDIR/'$WORKDIR'/g" $SCHEDULER.sh
sed -i "s/NAME/$NAME/g" $SCHEDULER.sh
sed -i "s/TASKS/$TASKS/g" $SCHEDULER.sh
sed -i "s/TIME/$TIME/g" $SCHEDULER.sh
sed -i "s/ERROR/$ERROR/g" $SCHEDULER.sh
sed -i "s/OUT/$OUT/g" $SCHEDULER.sh
sed -i "s/PPORT/$PREDEFINED_PORT/g" $SCHEDULER.sh

fi

CONNECTION=$( scp $SCHEDULER.sh $USERNAME@$CLUSTER:$WORKDIR )
CONNECTION=$( scp scripts/start_containers_test.sh $USERNAME@$CLUSTER:$WORKDIR )

if [ "$?" -eq "0" ]
then
        echo "Successfully uploaded jobscript!"
        rm $SCHEDULER.sh
else
        echo "Cluster connection error..."
        echo "Exiting"
        exit
fi

#JOB SUBMISSION


CONNECTION=$( ssh $USERNAME@$CLUSTER "cd $WORKDIR; sbatch $SCHEDULER.sh" )

if [ "$?" -eq "0" ]
then
        echo "Successfully submitted job"
else
        echo "Cluster connection error..."
        echo "Exiting"
        exit
fi
